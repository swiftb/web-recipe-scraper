from recipe_scrapers import scrape_me, WebsiteNotImplementedError, NoSchemaFoundInWildMode
from flask import Flask, request, jsonify
get_recipe = Flask(__name__)

@get_recipe.route('/getrecipe/', methods=['GET'])
def respond():
    # Retrieve the url from url parameter
    recurl = request.args.get("recipe-url", None)

    caughtRec = True
    scraper = {}
    #except Exception as e:
    #print(e)
    try:
        scraper = scrape_me(recurl)
    except WebsiteNotImplementedError:
        try:
            scraper = scrape_me(recurl,wild_mode=True)
        except NoSchemaFoundInWildMode:
            caughtRec = False
    
    response = {}
    if caughtRec == True:
        response["found_recipe"] = True
        response["title"] = scraper.title()
        response["total_time"] = scraper.total_time()
        response["yields"] = scraper.yields()
        response["ingredients"] = scraper.ingredients()
        response["instructions"] = scraper.instructions()
        response["image"] = scraper.image()
        response["host"] = scraper.host()
        #response["links"] = f"{scraper.links()}"
        response["nutrients"] = scraper.nutrients()
    else:
        response["found_recipe"] = False

    # Return the response in json format
    return response

# A welcome message to test our server
@get_recipe.route('/')
def index():
    return "<h1>Welcome to the Recipe Scraper</h1><p>To scrape a recipe, append /getrecipe/?recipe-url=[RECIPEURL] to the url</p>"

if __name__ == '__main__':
    # Threaded option to enable multiple instances for multiple user access support
    get_recipe.run(threaded=True, port=5000)